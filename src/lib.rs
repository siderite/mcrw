/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use std::io::{Error, ErrorKind, Read, Write, Result};
use std::mem::size_of;

pub const VAR_INT_MAX_SIZE: usize = var_int_size(!0);
pub const VAR_LONG_MAX_SIZE: usize = var_long_size(!0);

macro_rules! fn_read_be {
    ($(($name:ident, $type:ty),)*) => {
        $(
            #[inline]
            fn $name(&mut self) -> Result<$type> {
                let mut buf = [0u8; size_of::<$type>()];
                self.read_exact(&mut buf)?;
                Ok(<$type>::from_be_bytes(buf))
            }
        )*
    }
}

macro_rules! fn_write_be {
    ($(($name:ident, $type:ty),)*) => {
        $(
            #[inline]
            fn $name(&mut self, value: $type) -> Result<()> {
                self.write_all(&value.to_be_bytes())
            }
        )*
    }
}

/// Extends [`Read`] with methods for reading minecraft data types. (For `std::io`.)
///
/// # Examples
///
/// Read a minecraft string from a [`Read`]:
///
/// ```rust
/// use mcrw::MCReadExt;
///
/// let mut vec = [0x04, 0x6d, 0x63, 0x72, 0x77];
/// assert_eq!("mcrw", vec.as_slice().read_string().unwrap());
/// ```
///
/// [`Read`]: https://doc.rust-lang.org/std/io/trait.Read.html
pub trait MCReadExt: Read {

    #[inline]
    fn read_bool(&mut self) -> Result<bool> {
        match self.read_ubyte() {
            Ok(0x00) => Ok(false),
            Ok(0x01) => Ok(true),
            Ok(v) => Err(Error::new(ErrorKind::InvalidInput, format!("Invalid Value: {:#X}", v))),
            Err(e) => Err(e),
        }
    }

    fn_read_be!(
        (read_byte, i8),
        (read_ubyte, u8),
        (read_short, i16),
        (read_ushort, u16),
        (read_int, i32),
        (read_long, i64),
        (read_float, f32),
        (read_double, f64),
    );

    #[inline]
    fn read_string(&mut self) -> Result<String> {
        let len = self.read_var_int()?;
        if len == 0 {
            return Ok(String::new());
        }

        let mut bytes = vec![0; len as usize];
        self.read_exact(&mut bytes)?;

        String::from_utf8(bytes).map_err(|_| Error::new(ErrorKind::InvalidInput, "Couldn't create string"))
    }

    #[inline]
    unsafe fn read_string_unchecked(&mut self) -> Result<String> {
        let len = self.read_var_int()?;
        if len == 0 {
            return Ok(String::new());
        }

        let mut bytes = vec![0; len as usize];
        self.read_exact(&mut bytes)?;

        Ok(String::from_utf8_unchecked(bytes))
    }

    #[inline]
    fn read_var_int(&mut self) -> Result<i32> {
        let mut x = 0i32;

        for shift in 0..VAR_INT_MAX_SIZE {
            let b = self.read_ubyte()? as i32;
            x |= (b & 0x7F) << (7 * shift);
            if (b & 0x80) == 0 {
                return Ok(x);
            }
        }

        // The number is too large to represent in a 32-bit value.
        Err(Error::new(ErrorKind::InvalidInput, "VarInt is too big"))
    }

    #[inline]
    fn read_var_long(&mut self) -> Result<i64> {
        let mut x = 0i64;

        for shift in 0..VAR_LONG_MAX_SIZE {
            let b = self.read_ubyte()? as i64;
            x |= (b & 0x7F) << (7 * shift);
            if (b & 0x80) == 0 {
                return Ok(x);
            }
        }

        // The number is too large to represent in a 64-bit value.
        Err(Error::new(ErrorKind::InvalidInput, "VarLong is too big"))
    }

    #[inline]
    fn read_position(&mut self) -> Result<(i32, i32, i32)> {
        let value = self.read_long()?;
        Ok(((value >> 38) as i32,
            ((((value as u64) << 26) as i64) >> 52) as i32,
            (((value as u64) << 38) as i64 >> 38) as i32))
    }
}

/// All types that implement `Read` get methods defined in `MCReadExt`
/// for free.
impl<R: Read + ?Sized> MCReadExt for R {}

/// Extends [`Write`] with methods for writing minecraft data types. (For `std::io`.)
///
/// # Examples
///
/// Write a minecraft string to a [`Write`]:
///
/// ```rust
/// use mcrw::MCWriteExt;
///
/// let mut vec = Vec::new();
/// vec.write_string("mcrw").unwrap();
/// assert_eq!(vec, [0x04, 0x6d, 0x63, 0x72, 0x77]);
/// ```
///
/// [`Write`]: https://doc.rust-lang.org/std/io/trait.Write.html
pub trait MCWriteExt: Write {

    #[inline]
    fn write_bool(&mut self, value: bool) -> Result<()> {
        self.write_ubyte(value as u8)
    }

    fn_write_be!(
        (write_byte, i8),
        (write_ubyte, u8),
        (write_short, i16),
        (write_ushort, u16),
        (write_int, i32),
        (write_long, i64),
        (write_float, f32),
        (write_double, f64),
    );

    #[inline]
    fn write_string(&mut self, value: &str) -> Result<()> {
        self.write_var_int(value.len() as i32)?;
        self.write_all(value.as_bytes())
    }

    #[inline]
    fn write_var_int(&mut self, value: i32) -> Result<()> {
        let mut temp = value as u32;
        let mut buf = [0u8; VAR_INT_MAX_SIZE];
        let mut size = 0;

        while temp > 0x7F {
            buf[size] = (temp | 0x80) as u8;
            size += 1;
            temp >>= 7;
        }

        buf[size] = temp as u8;
        self.write_all(&buf[..=size])
    }

    #[inline]
    fn write_var_long(&mut self, value: i64) -> Result<()> {
        let mut temp = value as u64;
        let mut buf = [0u8; VAR_LONG_MAX_SIZE];
        let mut size = 0;

        while temp > 0x7F {
            buf[size] = (temp | 0x80) as u8;
            size += 1;
            temp >>= 7;
        }

        buf[size] = temp as u8;
        self.write_all(&buf[..=size])
    }

    #[inline]
    fn write_position(&mut self, x: i32, y: i32, z: i32) -> Result<()> {
        let value: i64 = (x as i64) << 38 | (((y as i64) & 0xFFF) << 26) | (z as i64) & 0x3FFFFFF;
        self.write_long(value)
    }
}

/// All types that implement `Write` get methods defined in `MCWriteExt`
/// for free.
impl<W: Write + ?Sized> MCWriteExt for W {}

/// Returns the size in bytes of this value as a var int.
pub const fn var_int_size(value: i32) -> usize {
    let mut value = value as u32;
    let mut size = 1;
    while { value >>= 7; value != 0 } {
        size += 1;
    }

    size
}

/// Returns the size in bytes of this value as a var long.
pub const fn var_long_size(value: i64) -> usize {
    let mut value = value as u64;
    let mut size = 1;
    while { value >>= 7; value != 0 } {
        size += 1;
    }

    size
}
