# mcrw

A library for working with the Minecraft data types.

## Example

```rust
use mcrw::MCReadExt;

let mut buf = [0x04, 0x6d, 0x63, 0x72, 0x77];
println!("{}", buf.as_slice().read_string().unwrap()); // Prints "mcrw"
```

```rust
use mcrw::MCWriteExt;

let mut vec = Vec::new();
vec.write_var_int(42).unwrap();
vec.write_string("mcrw").unwrap();
```
