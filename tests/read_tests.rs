use mcrw::{self, MCReadExt};

#[test]
fn read_bool_true() {
    const BUF: [u8; 1] = [1];
    assert!(BUF.as_slice().read_bool().unwrap());
}

#[test]
fn read_bool_false() {
    const BUF: [u8; 1] = [0];
    assert!(!BUF.as_slice().read_bool().unwrap());
}

#[test]
fn read_bool_invalid() {
    const BUF: [u8; 1] = [0x10];
    assert!(BUF.as_slice().read_bool().is_err());
}

macro_rules! read_var_int_tests {
    ($($name:ident: ($expected:literal, $input:expr),)*) => {
        $(
            #[test]
            fn $name() {
                let value = $input.as_slice().read_var_int().unwrap();
                assert_eq!(value, $expected);
            }
        )*
    }
}

read_var_int_tests! {
    read_var_int0: (0, [0x00]),
    read_var_int1: (1, [0x01]),
    read_var_int2: (2, [0x02]),
    read_var_int3: (127, [0x7f]),
    read_var_int4: (128, [0x80, 0x01]),
    read_var_int5: (255, [0xff, 0x01]),
    read_var_int6: (2147483647, [0xff, 0xff, 0xff, 0xff, 0x07]),
    read_var_int7: (-1, [0xff, 0xff, 0xff, 0xff, 0x0f]),
    read_var_int8: (-2147483648, [0x80, 0x80, 0x80, 0x80, 0x08]),
}

macro_rules! read_var_long_tests {
    ($($name:ident: ($expected:literal, $input:expr),)*) => {
        $(
            #[test]
            fn $name() {
                let value = $input.as_slice().read_var_long().unwrap();
                assert_eq!(value, $expected);
            }
        )*
    }
}

read_var_long_tests! {
    read_var_long0: (0, [0x00]),
    read_var_long1: (1, [0x01]),
    read_var_long2: (2, [0x02]),
    read_var_long3: (127, [0x7f]),
    read_var_long4: (128, [0x80, 0x01]),
    read_var_long5: (255, [0xff, 0x01]),
    read_var_long6: (2147483647, [0xff, 0xff, 0xff, 0xff, 0x07]),
    read_var_long7: (9223372036854775807, [0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f]),
    read_var_long8: (-1, [0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x01]),
    read_var_long9: (-2147483648, [0x80, 0x80, 0x80, 0x80, 0xf8, 0xff, 0xff, 0xff, 0xff, 0x01]),
    read_var_long10: (-9223372036854775808, [0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x01]),
}
