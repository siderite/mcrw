use mcrw;

macro_rules! var_int_size_tests {
    ($($name:ident: ($input:literal, $expected:literal),)*) => {
        $(
            #[test]
            fn $name() {
                assert_eq!(mcrw::var_int_size($input), $expected);
            }
        )*
    }
}

var_int_size_tests! {
    var_int_size0: (0, 1),
    var_int_size1: (1, 1),
    var_int_size2: (2, 1),
    var_int_size3: (127, 1),
    var_int_size4: (128, 2),
    var_int_size5: (255, 2),
    var_int_size6: (2147483647, 5),
    var_int_size7: (-1, 5),
    var_int_size8: (-2147483648, 5),
}

macro_rules! var_long_size_tests {
    ($($name:ident: ($input:literal, $expected:literal),)*) => {
        $(
            #[test]
            fn $name() {
                assert_eq!(mcrw::var_long_size($input), $expected);
            }
        )*
    }
}

var_long_size_tests! {
    var_long_size0: (0, 1),
    var_long_size1: (1, 1),
    var_long_size2: (2, 1),
    var_long_size3: (127, 1),
    var_long_size4: (128, 2),
    var_long_size5: (255, 2),
    var_long_size6: (2147483647, 5),
    var_long_size7: (9223372036854775807, 9),
    var_long_size8: (-1, 10),
    var_long_size9: (-2147483648, 10),
    var_long_size10: (-9223372036854775808, 10),
}
