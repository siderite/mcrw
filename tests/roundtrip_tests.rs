use std::mem::size_of;

use quickcheck_macros::quickcheck;

use mcrw::{self, MCReadExt, MCWriteExt};

#[quickcheck]
fn write_position_roundtrip(mut x: i32, mut y: i32, mut z: i32) -> bool {
    x = x.clamp(-33554432, 33554431);
    y = y.clamp(-2048, 2047);
    z = z.clamp(-33554432, 33554431);
    let mut buf = [0u8; size_of::<i64>()];
    buf.as_mut_slice().write_position(x, y, z).unwrap();
    let (x2, y2, z2) = buf.as_slice().read_position().unwrap();
    x == x2 && y == y2 && z == z2
}

#[test]
fn write_position_negative() {
    let (x, y, z) = (-1, -1, -1);
    let mut buf = [0u8; size_of::<i64>()];
    buf.as_mut_slice().write_position(x, y, z).unwrap();
    let (x2, y2, z2) = buf.as_slice().read_position().unwrap();
    assert_eq!(x, x2);
    assert_eq!(y, y2);
    assert_eq!(z, z2);
}

#[test]
fn write_position_min() {
    let (x, y, z) = (-33554432, -2048, -33554432);
    let mut buf = [0u8; size_of::<i64>()];
    buf.as_mut_slice().write_position(x, y, z).unwrap();
    let (x2, y2, z2) = buf.as_slice().read_position().unwrap();
    assert_eq!(x, x2);
    assert_eq!(y, y2);
    assert_eq!(z, z2);
}
