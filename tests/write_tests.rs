use mcrw::{self, MCWriteExt};

#[test]
fn write_bool_true() {
    let mut buf = Vec::with_capacity(1);
    buf.write_bool(true).unwrap();
    assert_eq!(buf[0], 0x01);
}

#[test]
fn write_bool_false() {
    let mut buf = Vec::with_capacity(1);
    buf.write_bool(false).unwrap();
    assert_eq!(buf[0], 0x00);
}

macro_rules! write_var_int_tests {
    ($($name:ident: ($input:literal, $expected:expr),)*) => {
    $(
        #[test]
        fn $name() {
            let mut buf = Vec::with_capacity(5);
            buf.write_var_int($input).unwrap();
            assert_eq!(buf, $expected);
        }
    )*
    }
}

write_var_int_tests! {
    write_var_int0: (0, [0x00]),
    write_var_int1: (1, [0x01]),
    write_var_int2: (2, [0x02]),
    write_var_int3: (127, [0x7f]),
    write_var_int4: (128, [0x80, 0x01]),
    write_var_int5: (255, [0xff, 0x01]),
    write_var_int6: (2147483647, [0xff, 0xff, 0xff, 0xff, 0x07]),
    write_var_int7: (-1, [0xff, 0xff, 0xff, 0xff, 0x0f]),
    write_var_int8: (-2147483648, [0x80, 0x80, 0x80, 0x80, 0x08]),
}

macro_rules! write_var_long_tests {
    ($($name:ident: ($input:literal, $expected:expr),)*) => {
    $(
        #[test]
        fn $name() {
            let mut buf = Vec::with_capacity(5);
            buf.write_var_long($input).unwrap();
            assert_eq!(buf, $expected);
        }
    )*
    }
}

write_var_long_tests! {
    write_var_long0: (0, [0x00]),
    write_var_long1: (1, [0x01]),
    write_var_long2: (2, [0x02]),
    write_var_long3: (127, [0x7f]),
    write_var_long4: (128, [0x80, 0x01]),
    write_var_long5: (255, [0xff, 0x01]),
    write_var_long6: (2147483647, [0xff, 0xff, 0xff, 0xff, 0x07]),
    write_var_long7: (9223372036854775807, [0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f]),
    write_var_long8: (-1, [0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x01]),
    write_var_long9: (-2147483648, [0x80, 0x80, 0x80, 0x80, 0xf8, 0xff, 0xff, 0xff, 0xff, 0x01]),
    write_var_long10: (-9223372036854775808, [0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x01]),
}
