#![feature(test)]
extern crate test;

use test::Bencher;
use test::bench::black_box;

use mcrw;
use mcrw::{MCWriteExt, VAR_INT_MAX_SIZE, VAR_LONG_MAX_SIZE};

#[bench]
fn write_var_int(b: &mut Bencher) {
    let mut buf = [0u8; VAR_INT_MAX_SIZE];
    b.iter(|| {
        black_box(buf.as_mut_slice()).write_var_int(-2147483648).unwrap();
    })
}

#[bench]
fn write_var_long(b: &mut Bencher) {
    let mut buf = [0u8; VAR_LONG_MAX_SIZE];
    b.iter(|| {
        black_box(buf.as_mut_slice()).write_var_long(-9223372036854775808).unwrap();
    })
}
