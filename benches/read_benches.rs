#![feature(test)]
extern crate test;

use test::Bencher;
use test::bench::black_box;

use mcrw;
use mcrw::MCReadExt;

#[bench]
fn read_var_int(b: &mut Bencher) {
    const VAL : [u8; 5] = [0x80, 0x80, 0x80, 0x80, 0x08];
    b.iter(|| {
        let _ = black_box(VAL.as_slice()).read_var_int().unwrap();
    })
}

#[bench]
fn read_var_long(b: &mut Bencher) {
    const VAL : [u8; 10] = [0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x01];
    b.iter(|| {
        let _ = black_box(VAL.as_slice()).read_var_long().unwrap();
    })
}
